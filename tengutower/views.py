# from django.http import HttpResponse
from django.shortcuts import render


def index(request):
    context = {
        'page_title': 'Global Index',
    }
    return render(request, 'index.html', context)
