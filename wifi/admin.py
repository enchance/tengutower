from django.contrib import admin
from .models import Taxonomy, Establishment, Address, Owner, Wifi, Project


class TaxAdmin(admin.ModelAdmin):
    list_display = ('taxread', 'taxvalue', 'taxtype')

# Register your models here.
admin.site.register(Taxonomy, TaxAdmin)
# admin.site.register(Establishment)
# admin.site.register(Address)
# admin.site.register(Owner)
# admin.site.register(Wifi)
# admin.site.register(Project)