from django.db import models
from django.conf import settings


class CommonFields(models.Model):
    # author = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    updated_at = models.DateTimeField(auto_now=True)
    created_at = models.DateTimeField(auto_now_add=True)

    class Meta:
        abstract = True


# activestat: active, inactive
# structure: hotel, restaurant, home, event, etc.
# location: BGC, Baguio
class Taxonomy(CommonFields):
    taxread = models.CharField(max_length=50)
    taxvalue = models.CharField(max_length=50)
    taxtype = models.CharField(max_length=20)
    is_active = models.BooleanField(default=True, verbose_name='Activate now')

    def __str__(self):
        return "{} ({})".format(self.taxread, self.taxtype)


# status: activestat
# location: location
class Establishment(CommonFields):
    establishment = models.CharField(max_length=50)
    location = models.ForeignKey(Taxonomy, on_delete=models.CASCADE, related_name="estloc")
    status = models.ForeignKey(Taxonomy, on_delete=models.CASCADE, related_name="eststat")

    def __str__(self):
        return self.loc


class Address(CommonFields):
    establishment = models.ForeignKey(Establishment, on_delete=models.CASCADE)
    opentime = models.DateTimeField(null=True)
    closetime = models.DateTimeField(null=True)
    city = models.CharField(max_length=100, blank=True)
    country = models.CharField(max_length=100, blank=True)


class Owner(CommonFields):
    first_name = models.CharField(max_length=191)
    last_name = models.CharField(max_length=191)

    def __str__(self):
        return "{} {}".format(self.first_name, self.last_name)


# wifistat: active
# wifistr: structure
class Wifi(CommonFields):
    title = models.CharField(max_length=50)
    ssid = models.CharField(max_length=50)
    passwd = models.CharField(max_length=50)
    wpspin = models.CharField(max_length=20, null=True, blank=True)
    notes = models.CharField(max_length=191, null=True, blank=True)
    encryption = models.ForeignKey(Taxonomy, on_delete=models.CASCADE, related_name="wifienc", null=True, blank=True)
    structure = models.ForeignKey(Taxonomy, on_delete=models.CASCADE, related_name="wifistr", null=True, blank=True)
    status = models.ForeignKey(Taxonomy, on_delete=models.CASCADE, related_name="wifistat", null=True, blank=True)
    is_hacked = models.BooleanField(default=False)
    is_mobile = models.BooleanField(default=False)
    is_router = models.BooleanField(default=True)

    def __str__(self):
        return "{}".format(self.ssid)


# projstat: noted, planning, development, active, inactive
class Project(CommonFields):
    project = models.CharField(max_length=191)
    description = models.TextField(null=True, blank=True)
    is_active = models.BooleanField(default=False)
    is_visible = models.BooleanField(default=True)
    # status = models.ForeignKey(Taxonomy, on_delete=models.CASCADE, related_name="projstat")

    def __str__(self):
        return self.project