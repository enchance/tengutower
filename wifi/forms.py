from django import forms


class WifiForm(forms.Form):
    title = forms.CharField(max_length=50, required=False)
    ssid = forms.CharField(max_length=50)
    password = forms.CharField(max_length=50, widget=forms.PasswordInput)
    # wpspin = forms.CharField(max_length=20)
    notes = forms.CharField(max_length=191)
    # encryption = forms.ForeignKey()
    # structure = forms.ForeignKey()
    # status = forms.ForeignKey()
    is_hacked = forms.BooleanField(widget=forms.CheckboxInput, required=False)
    # is_mobile = forms.BooleanField()
    # is_router = forms.BooleanField()