from django.shortcuts import render
from django.http import HttpResponse
from .forms import WifiForm
from .models import Wifi

def index(request):
    context = {
        'page_title': 'Wifi Index'
    }
    return render(request, 'wifi/index.html', context)


def new(request):
    if request.method == 'POST':
        form = WifiForm(request.POST)

        if form.is_valid():
            # Init
            title = form.cleaned_data['title']
            ssid = form.cleaned_data['ssid']
            password = form.cleaned_data['password']
            notes = form.cleaned_data['notes']
            is_hacked = form.cleaned_data['is_hacked']

            data = Wifi(title=title, ssid=ssid, passwd=password, notes=notes, is_hacked=is_hacked)
            data.save()

            return HttpResponse('Success!')

    else:
        form = WifiForm()

    context = {
        'form': form
    }
    return render(request, 'wifi/new.html', context)


def details(request, wifiid):
    return HttpResponse('Details for {}'.format(wifiid));


def edit(request, wifiid):
    return HttpResponse('Edit wifi {}'.format(wifiid));


def delete(request, wifiid):
    return HttpResponse('Delete wifi {}'.format(wifiid));


def about(request):
    return HttpResponse('Static: About Wifi');


