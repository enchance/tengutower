from django.urls import path
from . import views

app_name = 'wifi'
urlpatterns = [
    path('', views.index, name="index"),
    path('<int:wifiid>/', views.details, name="details"),
    path('<int:wifiid>/edit', views.edit, name="edit"),
    path('<int:wifiid>/delete', views.delete, name="delete"),
    path('about/', views.about, name="about"),
    path('new/', views.new, name="new",)
]
